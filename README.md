## Demo Link

https://blogs-website-drab.vercel.app/

## Project: blogs-website

## Description

This is a React-based blog application that provides a platform for you to create, manage, and share your blog posts. It leverages Next.js for server-side rendering and static site generation for optimal performance and scalability.

## Features:

Blog Post Creation and Management: Create new blog posts, edit existing ones, and manage their content effectively.
User Interface: Enjoy a user-friendly and responsive interface built with React and styled using Tailwind CSS and Shaden UI (optional).
Internationalization (i18n): Support for multiple languages to enhance accessibility and reach a wider audience (using next-intl) (AR | EN).
Form Validation: Ensure data integrity with robust form validation powered by react-hook-form.
Theming: Tailor the look and feel of your blog according to your preferences (using next-themes).
Technology Stack:

## Frontend:

React: The core framework for building dynamic user interfaces.
Next.js: Provides server-side rendering and static site generation capabilities.
Tailwind CSS: A utility-first CSS framework for rapid and responsive styling.
Shaden UI (Optional): A React component library based on Tailwind CSS (for a consistent design language).
react-hook-form: Manages form state and validation efficiently.
next-intl: Enables easy implementation of multilingual support.
next-themes: Offers built-in theming support for Next.js applications.
zod: Offers type-safe data validation.
date-fns: Provides a utility library for working with dates.
Others: Additional libraries may be included for specific functionalities.
Development Tools:
TypeScript: Enhances code maintainability and type safety.
ESLint: Ensures code quality and adheres to best practices.
PostCSS: Enables the use of preprocessors with CSS.
Autoprefixer: Adds vendor prefixes for cross-browser compatibility.
Others: Additional tools may be used for testing, linting, or debugging.
Installation

Clone this repository: git clone https://gitlab.com/Hadee13/blogs-website
Install dependencies: npm install (or yarn install)
Development

Start the development server: npm run dev (or yarn dev)
The application will be available at http://localhost:3000 (default port) by default.
Build

Create an optimized production build: npm run build (or yarn build)
The build output can be found in the ./.next directory.
Deployment

Follow the deployment instructions specific to your chosen hosting platform (e.g., Vercel, Netlify).
The production build output (from the ./.next directory) is typically used for deployment.
Additional Notes

Shaden UI usage is optional and provides a pre-built component library based on Tailwind CSS.
Refer to the documentation of individual libraries for detailed usage and configuration options

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`].

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

-   [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
-   [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
