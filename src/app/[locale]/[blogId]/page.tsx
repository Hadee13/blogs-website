"use client";
import DateDisplay from "@/components/Date";
import { Button } from "@/components/ui/button";
import { CardDescription, CardTitle } from "@/components/ui/card";
import { IBlog } from "@/interfaces";
import { useBlogs } from "@/providers/blogs-provider";
// import { IBlog } from "@/interfaces";
// import { getBlog } from "@/utils/actions";
import Link from "next/link";
export default function Page({
    params: { blogId },
}: {
    params: { blogId: string };
}) {
    // const { id, title, description, date }: IBlog = await getBlog(blogId);
    const { blogs } = useBlogs();
    const blog = blogs.find((blog: IBlog) => blog.id === parseInt(blogId));
    if (blog === undefined) throw new Error("failed");
    return (
        <div className="container space-y-7 mb-7 mt-32">
            <Link href={`/`}>
                <Button variant={"secondary"}>Go Home</Button>
            </Link>
            <div className="p-7 flex flex-col gap-7 border border-primary rounded-md shadow-primary hover:shadow-primary shadow-sm bg-[#fdf3fb59] dark:bg-background">
                <CardTitle>{blog?.title}</CardTitle>
                <DateDisplay date={blog?.date} />
                <CardDescription>{blog?.description}</CardDescription>
            </div>
        </div>
    );
}
