import { blogs } from "../data";
export async function GET(
    request: Request,
    { params }: { params: { blogId: string } }
) {
    const blog = blogs.find((blog) => blog.id === parseInt(params.blogId));
    return Response.json(blog);
}
