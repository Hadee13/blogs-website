import { sortBlogs } from "@/lib/utils";
import { blogs } from "./data";
export async function GET() {
    const sortedBlogs = sortBlogs(blogs)
    return Response.json(sortedBlogs);
}

export async function POST(request: Request) {
    const formDate = await request.json();
    const newBlog = {
        id: blogs.length + 1,
        title: formDate.title,
        description: formDate.description,
        date: new Date().toISOString(),
    };
    blogs.push(newBlog);
    return new Response(JSON.stringify(newBlog), {
        headers: {
            "Content-Type": "application/json",
        },
        status: 201,
    });
}
