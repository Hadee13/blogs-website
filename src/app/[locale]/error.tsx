"use client";

import { Button } from "@/components/ui/button";

export default function GlobalError({
    error,
    reset,
}: {
    error: Error & { digest?: string };
    reset: () => void;
}) {
    console.log(error);
    return (
        <html>
            <body className="h-screen flex flex-col justify-center items-center gap-3">
                <h2>Something went wrong! {error.message}</h2>
                <Button variant={"default"} onClick={() => reset()}>Try again</Button>
            </body>
        </html>
    );
}
