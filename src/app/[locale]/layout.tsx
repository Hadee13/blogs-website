import "./globals.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { ThemeProvider } from "@/providers/theme-provider";
import { IRootLayout } from "@/interfaces";
import useTextDirection from "@/hooks/useTextDirection";
import Header from "@/components/layout/Header";
import { useTranslations } from "next-intl";
import { BlogsProvider } from "@/providers/blogs-provider";
import { Toaster } from "react-hot-toast";
export const baseUrl = "https://blogs-website-drab.vercel.app/"; // Replace with your actual base URL
const metadataBase = new URL(baseUrl);
const inter = Inter({ subsets: ["latin"] });
export const metadata: Metadata = {
    metadataBase: metadataBase,
    title: "Blogs Website",
    description:
        "Software Engineer, always looking for new challenges and knowledge, I have the ability to quickly learn new stuff. I code in JavaScript, especially ReactJS and NextJS.",
    icons: "/next.svg",
    openGraph: {
        title: "Blogs Website",
        description:
            "Software Engineer, always looking for new challenges and knowledge, I have the ability to quickly learn new stuff. I code in JavaScript, especially ReactJS and NextJS.",
        url: metadataBase,
        images: [
            {
                url: "/image.jpg",
                width: 1200,
                height: 630,
                href: "https://blogs-website-drab.vercel.app/image.jpg",
                alt: "Open Graph image",
            },
        ],
        type: "website",
        emails: "hadeel13ibrahim@gmail.com",
    },
};

export default function RootLayout({
    children,
    params: { locale },
}: Readonly<IRootLayout>) {
    const direction = useTextDirection(locale);
    const t = useTranslations("LINKS");
    // console.log(t);
    return (
        <html lang={locale} dir={direction} suppressHydrationWarning>
            <body className={`${inter.className}`}>
                <ThemeProvider
                    attribute="class"
                    defaultTheme="system"
                    enableSystem
                    disableTransitionOnChange
                    themes={[
                        "dark",
                        "light",
                        "yellow",
                        "orange",
                        "blue",
                        "green",
                        "violet",
                    ]}
                >
                    <BlogsProvider>
                        <Header />
                        <Toaster />
                        {children}
                    </BlogsProvider>
                </ThemeProvider>
            </body>
        </html>
    );
}
