"use client";
const loading = () => {
    return (
        <div className="flex justify-center items-center min-h-screen">
            <div className="custom-loader !border-r-primary"></div>
        </div>
    );
};

export default loading;
