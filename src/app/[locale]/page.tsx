"use client"
// import { useBlogs } from "@/providers/blogs-provider";
import { BlogCard } from "@/components/BlogCard";
import BlogForm from "@/components/BlogForm";
import { IBlog } from "@/interfaces";
import { useBlogs } from "@/providers/blogs-provider";
// import { getBlogs } from "@/utils/actions";

export default function Page() {
    // const blogs = await getBlogs();
    const { blogs } = useBlogs();
    // console.log(blogs);
    return (
        <main className="container flex flex-col w-full gap-9 mb-9 mt-40">
            <BlogForm />
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-9">
                {blogs?.map((blog: IBlog) => (
                    <BlogCard
                        key={blog?.id}
                        // id={blog?.id.toString()}
                        id={blog?.id}
                        title={blog?.title}
                        description={blog?.description}
                        date={blog?.date}
                    />
                ))}
            </div>
        </main>
    );
}
