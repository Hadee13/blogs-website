"use client";

export default function NotFound() {
    return <h1 className="mt-10 font-semibold">Something went wrong!</h1>;
}
