import { Button } from "@/components/ui/button";
import {
    Card,
    CardContent,
    CardDescription,
    CardFooter,
    CardHeader,
    CardTitle,
} from "@/components/ui/card";
import { ListCollapse } from "lucide-react";
import { IBlog } from "@/interfaces";
import Link from "next/link";
import DateDisplay from "./Date";

export function BlogCard({ id, title, description, date }: IBlog) {
    // console.log({ id, title, description, date })
    return (
        <Card className="flex flex-col justify-between shadow-primary hover:shadow-primary shadow-sm hover:shadow-md bg-[#fdf3fb59] dark:bg-background border-primary">
            <Link href={`/${id}`}>
                <CardHeader>
                    <CardTitle className="truncate">{title}</CardTitle>
                </CardHeader>
                <CardContent className="space-y-5">
                    <DateDisplay date={date} />
                    <CardDescription className="line-clamp-3 text-justify indent-0 hover:indent-1">
                        {description}
                    </CardDescription>
                </CardContent>
            </Link>
            <CardFooter className="flex justify-end items-end gap-1">
                <Link href={`/${id}`}>
                    <Button variant={"outline"}>
                        <ListCollapse />
                    </Button>
                </Link>
            </CardFooter>
        </Card>
    );
}
