"use client";
import toast from "react-hot-toast";
import {
    Dialog,
    DialogContent,
    DialogFooter,
    DialogHeader,
    DialogTitle,
    DialogTrigger,
} from "@/components/ui/dialog";
import {
    Form,
    FormControl,
    // FormDescription,
    FormField,
    FormItem,
    FormLabel,
    FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { BlogFormValues, blogFormSchema } from "@/schema";
import { zodResolver } from "@hookform/resolvers/zod";
import { Plus } from "lucide-react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import Spinner from "./Spinner";
import { Button } from "./ui/button";
import { Textarea } from "./ui/textarea";
// import { createBlog } from "@/utils/actions";
import { useBlogs } from "@/providers/blogs-provider";
const BlogForm = () => {
    const { createBlog } = useBlogs();
    const [open, setOpen] = useState(false);
    // useSearchParams();
    const defaultValues: Partial<BlogFormValues> = {
        title: "",
        description: "",
        // date: new Date(),
    };
    const form = useForm<BlogFormValues>({
        resolver: zodResolver(blogFormSchema),
        defaultValues,
        mode: "onChange",
    });
    const onSubmit = async (formDate: BlogFormValues) => {
        console.log(formDate);
        const data = await createBlog(formDate);
        if (data) {
            setOpen(false);
            toast.success("successfully completed");
            form.reset();
        } else toast.error("error");
    };
    return (
        <Dialog open={open} onOpenChange={setOpen}>
            <DialogTrigger asChild className="ml-auto">
                <Button>
                    <Plus size={14} className="mr-1" />
                    New Blog
                </Button>
            </DialogTrigger>
            <DialogContent className="sm:max-w-[425px]">
                <DialogHeader>
                    <DialogTitle>Add a new Blog</DialogTitle>
                </DialogHeader>
                <div className="py-4">
                    <Form {...form}>
                        <form
                            onSubmit={form.handleSubmit(onSubmit)}
                            className="space-y-8"
                        >
                            <FormField
                                control={form.control}
                                name="title"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>Title</FormLabel>
                                        <FormControl>
                                            <Input placeholder="" {...field} />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                            <FormField
                                control={form.control}
                                name="description"
                                render={({ field }) => (
                                    <FormItem>
                                        <FormLabel>Short Description</FormLabel>
                                        <FormControl>
                                            <Textarea
                                                placeholder="Tell us a little bit about yourself"
                                                className="resize-none"
                                                {...field}
                                            />
                                        </FormControl>
                                        <FormMessage />
                                    </FormItem>
                                )}
                            />
                            <DialogFooter>
                                <Button
                                    type="submit"
                                    className="space-x-2"
                                    disabled={
                                        !form.formState.isDirty ||
                                        !form.formState.isValid ||
                                        form.formState.isSubmitting
                                    }
                                >
                                    {form.formState.isSubmitting ? (
                                        <>
                                            <Spinner />
                                            Saving
                                        </>
                                    ) : (
                                        "Save"
                                    )}
                                </Button>
                            </DialogFooter>
                        </form>
                    </Form>
                </div>
            </DialogContent>
        </Dialog>
    );
};

export default BlogForm;
