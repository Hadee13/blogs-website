import { CalendarDays } from "lucide-react";
import { useLocale } from "next-intl";

const DateDisplay = ({ date }: { date: string }) => {
    const localActive = useLocale();
    return (
        <div className="flex flex-row gap-1 text-slate-400">
            <CalendarDays size={20} />
            <div>
                {new Date(date).toLocaleDateString(localActive, {
                    month: "long",
                    day: "2-digit",
                    year: "numeric",
                })}
            </div>
        </div>
    );
};

export default DateDisplay;
