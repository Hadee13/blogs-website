import Link from "next/link";
import { Button } from "./ui/button";

export default function HomeBtn() {
    return (
        <Link href={`/`}>
            <Button variant={"secondary"}>Go Home</Button>
        </Link>
    );
}
