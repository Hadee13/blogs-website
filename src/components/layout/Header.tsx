"use client";
import { useEffect, useState } from "react";
import Logo from "./Logo";
import { ModeToggle } from "./ModeToggle";
import LocalSwitcher from "./LocalSwitcher";

export default function Header() {
    const [header, setHeader] = useState<boolean>(true);
    
    useEffect(() => {
        const handleScroll = () => {
            window.scrollY > 50 ? setHeader(true) : setHeader(false);
        };
        window.addEventListener("scroll", handleScroll);
        // Clean up the event listener
        return () => {
            window.removeEventListener("scroll", handleScroll);
        };
    }, []);
    return (
        <div
            className={`${header ? "shadow-md !py-3 dark:!bg-secondary" : ""} 
            sticky top-0 py-4 z-30 transition-all bg-[#fdf3fb] dark:bg-transparent w-full `}
        >
            <div className="container flex items-center justify-between">
                <Logo />
                <div className="flex justify-center items-center gap-x-7">
                    <ModeToggle />
                    <LocalSwitcher />
                </div>
            </div>
        </div>
    );
}
