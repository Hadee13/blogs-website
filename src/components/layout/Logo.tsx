import Link from "next/link";
export default function Logo() {
    return (
        <Link href={"/"} className="text-2xl font-extrabold">
            B<span className="text-primary">logs</span>
        </Link>
    );
}
