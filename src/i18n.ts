// import { notFound } from "next/navigation";
// import { getRequestConfig } from "next-intl/server";

// // Can be imported from a shared config
// const locales = ["en", "ar"];

// export default getRequestConfig(async ({ locale }) => {
//   // Validate that the incoming `locale` parameter is valid
//   if (!locales.includes(locale as any)) notFound();

//   return {
//     messages: (await import(`../messages/${locale}.json`)).default,
//   };
// });
import { notFound } from "next/navigation";
import { getRequestConfig } from "next-intl/server";
import { locales } from "./config";

export default getRequestConfig(async ({ locale }) => {
    // Validate that the incoming `locale` parameter is valid
    if (!locales.includes(locale as any)) notFound();

    return {
        messages: (
            await (locale === "en"
                ? import("../messages/en.json") // When using Turbopack, this will enable HMR for `en`
                : import(`../messages/${locale}.json`))
                // : import(`../messages/${locale}.json`))
        ).default,
    };
});
