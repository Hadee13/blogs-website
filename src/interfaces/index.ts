export interface IRootLayout {
    children: React.ReactNode;
    params: {
        locale: string;
    };
}
export interface IBlog {
    id: number;
    title: string;
    description: string;
    date: string; 
}
