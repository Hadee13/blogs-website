"use client";
import { ReactNode, useContext, useState, createContext } from "react";
import { IBlog } from "@/interfaces"; // Assuming you have an IBlog interface
import _blogs from "../../data/blogs.json";
import { BlogFormValues } from "@/schema";
import { sortBlogs } from "@/lib/utils";

const BlogsContext = createContext<{
    blogs: IBlog[];
    createBlog: (formDate: BlogFormValues) => IBlog;
} | null>(null);

export const BlogsProvider = ({ children }: { children: ReactNode }) => {
    // ... rest of your code (potentially fetching or updating blogs)
    const [blogs, setBlogs] = useState<IBlog[]>(_blogs);
    const createBlog = (formDate: BlogFormValues): IBlog => {
        const newBlog: IBlog = {
            id: blogs.length + 1,
            title: formDate.title,
            description: formDate.description,
            date: new Date().toISOString(),
        };
        setBlogs([...blogs, newBlog]);
        return newBlog;
    };
    return (
        <BlogsContext.Provider value={{ blogs: sortBlogs(blogs), createBlog }}>
            {children}
        </BlogsContext.Provider>
    );
};
export const useBlogs = () => {
    const value = useContext(BlogsContext);
    // Type assertion (optional) for stricter type checking within useBlogs
    if (!value) {
        throw new Error("useBlogs must be used within BlogsProvider");
    }
    const { blogs, createBlog } = value;
    return { blogs, createBlog };
};
