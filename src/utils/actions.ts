"use server";
import { baseUrl } from "@/app/[locale]/layout";
// import { redirect } from 'next/navigation'
import { revalidatePath, revalidateTag } from "next/cache";
export async function getBlogs() {
    const res = await fetch(`${baseUrl}ar/api/`, {
        cache: "no-cache",
        // next: { revalidate: 250 },
    });
    if (!res.ok) {
        throw new Error("Failed to fetch data");
    }
    const data = await res.json();
    return data;
}
export async function getBlog(blogId: string) {
    const res = await fetch(`${baseUrl}ar/api/${blogId}`, {
        // cache: "no-cache",
        next: { revalidate: 250 },
    });
    if (!res.ok) {
        throw new Error("Failed to fetch data");
    }
    const data = await res.json();
    return data;
}
export async function createBlog(formData: {
    title: string;
    description: string;
}) {
    try {
        // Call database
        const response = await fetch(`${baseUrl}ar/api`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(formData),
        });
        revalidateTag("collection");
        revalidatePath("/");
        const data = await response.json();
        return data;
    } catch (error) {
        // Handle errors
        if (!error) {
            // This will activate the closest `error.js` Error Boundary
            // throw new Error("Failed to fetch data");
        }
    }
    revalidatePath("/"); // Update cached posts
    // redirect(`/`)
}
